py3_formula = '''
# Generated using NuPL\n\n
<[{(src)}]>
'''
cpp_formula = '''
// Generated using NuPL
// The `main` function indentation may be wrong.
#include <cstdio>
#include <iostream>

using namespace std;

void print() {
  string str[2] = {"abc", "def"};
  string sep = "<SEP>";
  string end = "<END>";
  string result = "";
  
  // Convert str, sep end to result...
  for (int i = 0; i < 2; i++) {
    std::cout << str[i]
    if i == len(str) - 1:
      std::cout << end
    else:
      std::cout << sep
  std::cout << result;
}

int main()
{
  <[{(src)}]>
}
'''
src_formulas = {'python3': py3_formula,
              'c++':     cpp_formula, }