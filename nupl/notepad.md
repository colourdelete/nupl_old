# Notepad

This is for the developer(s)' random notes.

```textx
PseudoComment: // TODO: Fix Comment and PseudoComment
  // /(?ms)#[^;]*;/
;
```
___

```nupl
print("HeEeEeEeEeEeEeElO");
#print("LoLoL");
print("Unnamed", end="<EOF>");
string = "STRING boi"
collection = [print("LOL");]
```
```nupl
print("HeEeEeEeEeEeEeElO");
print("Unnamed", end="<EOF>");
string = "STRING boi";

collection = [print();]
print_func = (string)[print(string, end="");]
print_func("string boi")
```

___

```cpp
#include <stdio.h>

#define abc 123 // abc is now 123

int func(int b); // define skeleton
int func(int b) // Function
{
    if (a == 1) {
    } else {
    }
    return (1+b);
}

int main() 
{
    printf("hello, world\n");
   
    return 0;
}



```
## Print Function
### Input
```cpp
string str[2] = {"abc", "def"};
string sep = "<SEP>";
string end = "<END>";
```
### Output
```
abc<SEP>def<END>
```
### Source Code
```cpp
#include <cstdio>
#include <iostream>

using namespace std

void print(******) {
  string str[2] = {"abc", "def"};
  string sep = "<SEP>";
  string end = "<END>";
  string result = "";
  
  // Convert str, sep end to result...
  for i in range(len(str)):
    std::cout << str[i]
    if i == len(str) - 1:
      std::cout << end
    else:
      std::cout << sep
  cout << result;
}
```